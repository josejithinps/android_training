package com.ffi.sampleapp;

/**
 * Created by u34744 on 1/9/2018.
 */

public class Constants {
    public static final String KEY_SECOND_ACTIVITY_MSG = "KEY_SECOND_ACTIVITY_MSG";
    public static final String KEY_SECOND_ACTIVITY_VAL_1 = "KEY_SECOND_ACTIVITY_VAL_1";
    public static final String KEY_SECOND_ACTIVITY_VAL_2 = "KEY_SECOND_ACTIVITY_VAL_2";
    public static final String KEY_SECOND_ACTIVITY_ADD_RES = "KEY_SECOND_ACTIVITY_ADD_RES";
}
