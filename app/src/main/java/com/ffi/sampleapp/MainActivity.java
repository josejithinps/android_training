package com.ffi.sampleapp;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity
        extends AppCompatActivity
        implements View.OnClickListener/*, CallbackStatus*/{

    private static final String TAG = MainActivity.class.getSimpleName();
    public static final int REQ_CODE_ADD_MUMBERS = 500;
    private Button mButton, mButtonGo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mButton = (Button) findViewById(R.id.btn_show);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v(TAG, "1---Hello world");
                Log.d(TAG, "2---Hello world");
            }
        });

        mButtonGo = (Button) findViewById(R.id.btn_go);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v(TAG, "1---Hello world");
                Log.d(TAG, "2---Hello world");
            }
        });

        mButton = (Button) findViewById(R.id.btn_show);
        mButton.setOnClickListener(this);
        mButtonGo = (Button) findViewById(R.id.btn_go);
        mButtonGo.setOnClickListener(this);
//        SecondActivity.callbackStatus = this;
//        SecondActivity.setCallbackStatus(this);
        CallbackStatus callbackStatus = new CallbackStatus() {
            @Override
            public void onUserClicked(int result) {

            }
        };
        SecondActivity.setCallbackStatus(callbackStatus);
    }

    @Override
    public void onClick(View view) {


        /*if (view.getId() == R.id.btn_show) {

        } else if (view.getId() == R.id.btn_go) {

        }*/
        switch (view.getId()) {
            case R.id.btn_show:
                //sjdfhsdhf
                showDialogWithMessge("This is the title", "This is the body");
                break;
            case R.id.btn_go:
                startAnotherActivity();
                startAnotherActivityToGetResult();
                break;
            default:
                break;
        }
    }

    public void showMessage(View view) {
        Log.d(TAG, "This is a tag");
        Toast.makeText(this, "THi sdfhsdjhf sd", Toast.LENGTH_SHORT);

    }

    public void showDialogWithMessge(/*Context context,*/ String title, String body) {
        AlertDialog.Builder builder = new AlertDialog.Builder(/*context*/this);
        builder.setTitle(title);
        builder.setMessage(body);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.show();
        /*new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(body)
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();*/
    }

    private void startAnotherActivity() {
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra(Constants.KEY_SECOND_ACTIVITY_MSG, 30);
        startActivity(intent);
    }

    private void startAnotherActivityToGetResult() {
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra(Constants.KEY_SECOND_ACTIVITY_VAL_1, 10);
        intent.putExtra(Constants.KEY_SECOND_ACTIVITY_VAL_2, 20);
        startActivityForResult(intent, REQ_CODE_ADD_MUMBERS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent dataRes) {
        super.onActivityResult(requestCode, resultCode, dataRes);
        if (resultCode == RESULT_OK) {
            Log.d(TAG, "Calc is good");
            if (requestCode == REQ_CODE_ADD_MUMBERS) {
//                int res = dataRes.getIntExtra();

            }
        } else {
            Log.d(TAG, "cal failed!");
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        startAnotherActivity();
    }

    /*@Override
    public void onUserClicked(int result) {
        int res = result;

    }*/
}
