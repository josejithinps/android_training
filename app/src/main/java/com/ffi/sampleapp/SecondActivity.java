package com.ffi.sampleapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SecondActivity extends AppCompatActivity {

    public static CallbackStatus callbackStatus;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Intent intent  = getIntent();
        int msg = intent.getIntExtra(Constants.KEY_SECOND_ACTIVITY_MSG, 0);
        int firstValue = intent.getIntExtra(Constants.KEY_SECOND_ACTIVITY_VAL_1, 0);
        int secondValue = intent.getIntExtra(Constants.KEY_SECOND_ACTIVITY_VAL_2, 0);
        int res = firstValue + secondValue;

        Intent result = new Intent();
        result.putExtra(Constants.KEY_SECOND_ACTIVITY_ADD_RES, res);

//        setResult(RESULT_OK,result);
//        finish();
        callbackStatus.onUserClicked(res);
    }

    public static CallbackStatus getCallbackStatus() {
        return callbackStatus;
    }

    public static void setCallbackStatus(CallbackStatus callbackStatus) {
        SecondActivity.callbackStatus = callbackStatus;
    }
}
